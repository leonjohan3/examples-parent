## Overview
This project is the parent pom.xml for various example projects related to https://bitbucket.org/leonjohan3/profile

## Tags
Java, Maven, Parent POMs, [Markdown](https://bitbucket.org/tutorials/markdowndemo).
